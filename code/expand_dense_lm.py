import torch
from torch import nn
import torch.nn.functional as F

from pytorch_pretrained_bert import BertTokenizer, BertModel
import pickle
import numpy as np
import os
from sklearn.utils import shuffle
import adabound

from utils import DataLoader

os.environ['CUDA_LAUNCH_BLOCKING'] = "1"
#os.environ['CUDA_VISIBLE_DEVICES'] = "3"


bertpath = "../../BERT_juman"

tokenizer = BertTokenizer.from_pretrained(bertpath, do_lower_case=False)

bert_trained = BertModel.from_pretrained(bertpath)
bert_trained.to('cuda')
bert_trained = torch.nn.DataParallel(bert_trained)

class DenseLM(nn.Module):
    def __init__(self, num_label):
        super(DenseLM, self).__init__()

        self.fc1 = nn.Linear(768, num_label)

    def forward(self, x):
        x = F.log_softmax(self.fc1(x), dim=1) 
        return x

size = "small"
with open("../data/split/"+size+"/train_source.txt", "r") as f:
    source = f.readlines()
print("source readed")

with open("../data/split/"+size+"/train_label.txt", "r") as f:
    label = f.readlines()
print("label readed") 
with open("../data/split/"+size+"/test_source.txt", "r") as f:
    valid_source = f.readlines()
print("valid_source readed")

with open("../data/split/"+size+"/test_label.txt", "r") as f:
    valid_label = f.readlines()
print("valid_label readed")


source = [s.replace("\n", "") for s in source]
label = [s.replace("\n", "") for s in label]
valid_source = [s.replace("\n", "") for s in valid_source]
valid_label = [s.replace("\n", "") for s in valid_label]

label2id ={}
id2label = tokenizer.vocab

for l in label:
    _id = tokenizer.convert_tokens_to_ids([l])
    label2id.setdefault(l, _id[0])
    id2label[_id[0]] = l

print("dict prepared")

def clean(sentence):
    sentence = tokenizer.tokenize(sentence)
    sentence = tokenizer.convert_tokens_to_ids(sentence)
    return sentence[:300]

label = [label2id[l] for l in label]
source = [clean(s) for s in source]
# [MASK] id is 4
mask = [s.index(4) for s in source]

print("train preprocess")
print("source:", source[0])
print("label:", label[0])
print("mask:", mask[0])

valid_label = [label2id[l] for l in valid_label]
valid_source = [clean(s) for s in valid_source]
valid_mask = [s.index(4) for s in valid_source]

print("valid preprocess")
print("source:", valid_source[0])
print("label:", valid_label[0])
print("mask:", valid_mask[0])


model = DenseLM(len(id2label))
model = model.to("cuda")

criterion = nn.CrossEntropyLoss(ignore_index=0, size_average=False).to("cuda")


log_train_loss = []
log_valid_loss = []

print(len(source))

batch_size = 128 
train_loader = DataLoader(source, label, mask, batch_size, bert_trained)
valid_loader = DataLoader(valid_source, valid_label, valid_mask, batch_size, bert_trained)

optimizer = adabound.AdaBound(model.parameters(), lr=1e-3, final_lr=0.1)
num_epochs=16

for epoch in range(num_epochs+1):
    print("epoch:", epoch)
    train_loss = 0.
    for X, y in train_loader:
        print(X.shape)
        model.train()
        pred_y = model(X)

        loss = criterion(pred_y, y.view(-1))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()        
        
        train_loss += loss.item()
    
    train_loss = train_loss / len(train_loader.data)
    print(f"epooch{epoch}'s train_loss:{train_loss}")
    log_train_loss.append(train_loss)


    valid_loss = 0.
    for X, y in valid_loader:
        model.eval()
        pred_y = model(X)

        loss = criterion(pred_y, y.view(-1))
        
        valid_loss += loss.item()
    
    valid_loss = valid_loss / len(valid_loader.data)
    print(f"epooch{epoch}'s valid_loss:{valid_loss}")
    log_valid_loss.append(valid_loss)

    #torch.save(model.state_dict(), "output/model"+str(epoch)+".pth") 

print("fin")
