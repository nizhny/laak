import torch
from pytorch_pretrained_bert import BertTokenizer, BertModel, BertForMaskedLM
import pickle
from operator import itemgetter
import numpy as np
import os
os.environ['CUDA_LAUNCH_BLOCKING'] = "1"

bertpath = "/nowxx/k.ishihara/BERT/Japanese_L-12_H-768_A-12_E-30_BPE"

tokenizer = BertTokenizer.from_pretrained(bertpath)

lm_model = BertForMaskedLM.from_pretrained(bertpath)
lm_model.to('cuda')

#fe_model = BertModel.from_pretrained(bertpath)
#fe_model.to('cuda')


with open("/nowxx/k.ishihara/lab-spring/data/test_source.txt","r") as f:
    source = f.readlines()
print("source readed")

with open("/nowxx/k.ishihara/lab-spring/data/test_label.txt","r") as f:
    label = f.readlines()
print("label readed")

idx = np.random.choice(np.arange(len(source)), 100000, replace=True)
source = itemgetter(*idx)(source)
label = itemgetter(*idx)(label)

def clean(sentence):
    if "<TGT>" not in sentence:
        print(sentence)
    sentence = sentence.replace("<TGT>", " [MASK] ")
    sentence = tokenizer.tokenize(sentence)
    index = sentence.index("[MASK]")
    sentence = tokenizer.convert_tokens_to_ids(sentence)
    return torch.tensor([sentence]), index

#embed = []
index = []
preds = []

source = [clean(s) for s in source]
   
for t, i in source:
    index.append(i)
    tokens = t.to("cuda")

    #segments = torch.zeros(t.shape[1]).to("cuda")
    segments = [0 for i in range(t.shape[1])]
    segments = torch.tensor([segments]).to("cuda")
    
    with torch.no_grad():
        lm_output = lm_model(tokens, segments).to("cpu")
    #    fe_output, _ = fe_model(tokens, segments)

    print(i)
    print(lm_output.shape)
    #preds.append({tokenizer.convert_ids_to_tokens([k]):output[0, i, k] for k in output[0, i].tolist()})

    pred_index = torch.argmax(lm_output[0, i]).item()
    preds.append(tokenizer.convert_ids_to_tokens([pred_index])[0])

#    for j in range(len(fe_output)):
#        fe_output[j] = fe_output[j].to("cpu")
#    embed.append(fe_output)

print(len(preds))
print(len(label))

#with open("output/embs.pkl", "wb") as f:
#    pickle.dump(embed, f)

#with open("output/index.pkl", "wb") as f:
#    pickle.dump(index, f)

with open("output/label.txt", "w") as f:
    f.write("".join(label))

with open("output/preds.txt", "w") as f:
    f.write("\n".join(preds)+"\n")
