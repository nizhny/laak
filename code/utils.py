import torch
from sklearn.utils import shuffle


class DataLoader(object):
    def __init__(self, X, y, mask, batch_size, bert_model):
        self.data = list(zip(X, y, mask))
        self.batch_size = batch_size
        self.reset()
        self.bert_model = bert_model

    def reset(self):
        self.data = shuffle(self.data)
        self.start_index = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.start_index >= len(self.data):
            self.reset()
            raise StopIteration()

        X, Y, mask = zip(*self.data[self.start_index:self.start_index+self.batch_size])
        max_seq = max([len(x) for x in X])
        X = [x+[0 for s in range(max_seq - len(x))] for x in X]
        X =torch.tensor(X, dtype=torch.long, device="cuda")

        with torch.no_grad():
            batch_embed, _ = self.bert_model(X, output_all_encoded_layers=False)

        batch_X = [batch_embed[i, m, :] for i, m in enumerate(mask)]
        batch_X = torch.stack(batch_X)
        batch_Y = torch.tensor(Y, dtype=torch.long, device="cuda")
         # ポインタを更新する
        self.start_index += self.batch_size
        return batch_X, batch_Y
