import io
from html.parser import HTMLParser

class MyHtmlStripper(HTMLParser):
    def __init__(self, s):
        super().__init__()
        self.sio = io.StringIO()
        self.feed(s)

    def handle_starttag(self, tag, attrs):
        pass

    def handle_endtag(self, tag):
        pass

    def handle_data(self, data):
        self.sio.write(data)

    @property
    def value(self):
        return self.sio.getvalue() 


def strip_tags(html):
    html = html.replace(" ", "")
    return html.replace("&emsp;", "")

def main():
	source = []
	with open("../data/test_source.txt", "r") as f:
		text = f.readline()
		while text:
			source.append(strip_tags(text[:-1]))
			text = f.readline()

	with open("../data/clean_source.txt", "w") as f:
		f.write("\n".join(source))
		

if __name__ == "__main__":
	main()
