import jaconv
import argparse
from pyknp import Jumanpp

juman = Jumanpp()

def owakati(sentence):
	res = juman.analysis(jaconv.h2z(sentence, digit=True, ascii=True))
	return " ".join([str(m.midasi) for m in res.mrph_list()]).replace("　", "")

def label():
	label = []
	with open("../data/test_label.txt", "r") as f:
		text = f.readline()
		while text:
			label.append(owakati(text[:-1]))
			text = f.readline()

	with open("../data/juman_label.txt", "w") as f:
		f.write("\n".join(label))
		

def source():
	source = []
	with open("../data/clean_source.txt", "r") as s, open("../data/juman_label.txt", "r") as l:
		text = s.readline()
		labels = l.readline()
		while text:
			try:
				mask = text.index("[MASK]")
			except Exception as e:
				print("ERROR:", text)
			

			before = owakati(text[:mask])
			after = owakati(text[mask+6:-1])
			print(labels.split())
			source.append(before+" "+ ("[MASK] " * len(labels.split()))+after)

			text = s.readline()
			labels = l.readline()

	with open("../data/juman_source.txt", "w") as f:
		f.write("\n".join(source))
		


if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("porpose", choices=["label", "source"])
	args = parser.parse_args()
	if args.porpose == "label":
		label()
	else:
		source()
