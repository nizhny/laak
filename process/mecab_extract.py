import sys
import MeCab

def main():
	m = MeCab.Tagger("-d /usr/lib/x86_64-linux-gnu/mecab/dic/mecab-ipadic-neologd")
	o = MeCab.Tagger("-d /usr/lib/x86_64-linux-gnu/mecab/dic/mecab-ipadic-neologd -Owakati")

	with open(sys.argv[1], "r") as f:
		text = f.read()
	
#souce
	def filter(c):
		return len([s for s in m.parse(c).split("\n")[:-2] if "固有名詞" in s.split("\t")[1]]) > 1
	source = [s for s in text.split("\n") if filter(s)]

#target
	def ext(c):
		return [s.split("\t")[0] for s in m.parse(c).split("\n")[:-2] if "固有名詞" in s.split("\t")[1]][1]

	masked = [s.replace(ext(s), "[MASK]") for s in source]
	with open("test_source.txt", "w") as f:
		f.write("\n".join(masked))
	
	label = [ext(s) for s in source]
	with open("test_label.txt", "w") as f:
		f.write("\n".join(label))
	print(len(masked), len(label))
	
		

if __name__ == "__main__":
	main()

